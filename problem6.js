function filterBMWandAUDI(inventory) {
    if(typeof inventory == "object" && inventory!== null && typeof inventory.length=='number') {
        const BMWandAUDI = [];
        for (const car of inventory) {
            if (car.car_make === "BMW" || car.car_make === "Audi") { 
                BMWandAUDI.push(car);
            }
        }
        return BMWandAUDI;
    } else {
        return ;
    }
    
}

module.exports = filterBMWandAUDI;