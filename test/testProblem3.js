const inventory = require("../inventory");
const setModelsAlphabetically = require("../problem3");

function testModelsAlphabetically(){
    const sortedList = setModelsAlphabetically(inventory);
    console.log(sortedList);
}

testModelsAlphabetically();