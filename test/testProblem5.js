const countCarsBeforeYear = require("../problem5");
const inventory = require("../inventory");

function testCountCarsBeforeYear(){
    const year = 2000;
    const count = countCarsBeforeYear(inventory, year);
    console.log(`Number of cars made before ${year}: ${count}`);
}

testCountCarsBeforeYear();