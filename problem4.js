function carYears(inventory){
    

    if(typeof inventory == "object" && inventory!== null && typeof inventory.length=='number') {
        const yearList = [] 
    
        for (let iterator=0; iterator<inventory.length; iterator++){
            yearList.push(inventory[iterator].car_year);
        }
        return yearList;
    } else {
        return ;
    }
}

module.exports = carYears;
