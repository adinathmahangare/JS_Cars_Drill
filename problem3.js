function setModelsAlphabetically(inventory){
    

    if(typeof inventory == "object" && inventory!== null && typeof inventory.length=='number') {
        const modelList = [];
        for (let car of inventory) {
            modelList.push(car.car_model)
        }

        for (let var1 = 0; var1 < modelList.length; var1++) {
            for (let var2 = 0; var2<modelList.length - var1 - 1; var2++) {
                if (modelList[var2] > modelList[var2 + 1]) {
                    let temp = modelList[var2];
                    modelList[var2] = modelList[var2 + 1];
                    modelList[var2 + 1] = temp;
                }
            }
        }
        
        return modelList;
    } else {
        return ;
    }
}

module.exports = setModelsAlphabetically;