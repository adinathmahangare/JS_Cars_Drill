const carYears = require("./problem4.js");

function carsBeforeYear(inventory, year) {

    if(typeof inventory == "object" && inventory!== null && typeof inventory.length=='number') {
        let count = 0;
        for (let iterator = 0; iterator< inventory.length; iterator++) {
            if (inventory[iterator].car_year < year) {
                count++;
            }
        }
        return count;
    } else {
        return ;
    }

}

module.exports = carsBeforeYear;